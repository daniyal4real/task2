package kz.aitu.project.controller;

import kz.aitu.project.entity.Authorization;
import kz.aitu.project.service.AuthorizationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AuthorizationController {

    private final AuthorizationService authorizationService;

    public AuthorizationController(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    @GetMapping("/api/authorizations")
    public ResponseEntity<?> getAllAuthorizations(){
        return ResponseEntity.ok(authorizationService.getAll());
    }

    @GetMapping("/api/authorizations/{id}")
    public List<Authorization> getAuthorizationById(@PathVariable long id){
        return authorizationService.getAuthorizationById(id);
    }

    @PostMapping("/api/authorizations")
    public ResponseEntity<?> createAuthorization(@RequestBody Authorization authorization){
        return ResponseEntity.ok(authorizationService.createAuthorization(authorization));
    }

    @PutMapping("/api/authorizations/{id}")
    public ResponseEntity<Authorization> updateAuthorizationById(@RequestBody Authorization authorization, @PathVariable long id){
        return ResponseEntity.ok(authorizationService.updateAuthorization(authorization, id));
    }

    @DeleteMapping("/api/authorizations/{id}")
    public void deleteAuthorizationById(@PathVariable long id){
        authorizationService.deleteAuthorizationById(id);
    }
}
