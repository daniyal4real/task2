package kz.aitu.project.controller;


import kz.aitu.project.entity.Case;
import kz.aitu.project.service.CaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CaseController {

    private final CaseService caseService;


    public CaseController(CaseService caseService) {
        this.caseService = caseService;
    }


    @GetMapping("/api/cases")
    public ResponseEntity<?> getAllCases(){
        return ResponseEntity.ok(caseService.getAllCases());
    }

    @GetMapping("/api/cases/{id}")
    public List<Case> getCaseById(@PathVariable long id){
        return caseService.getCaseById(id);
    }


    @PostMapping("/api/cases")
    public ResponseEntity<?> createCase(@RequestBody Case cases){
        return ResponseEntity.ok(caseService.createCase(cases));
    }

    @PutMapping("/api/cases/{id}")
    public ResponseEntity<?> updateCaseById(@RequestBody Case cases, @PathVariable long id){
        return ResponseEntity.ok(caseService.updateCaseById(cases, id));
    }

    @DeleteMapping("/api/cases/{id}")
    public void deleteCaseById( @PathVariable long id){
        caseService.deleteCaseById(id);
    }

}
