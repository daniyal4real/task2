package kz.aitu.project.controller;


import kz.aitu.project.entity.CaseIndex;
import kz.aitu.project.service.CaseIndexService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CaseIndexController {

    private final CaseIndexService caseIndexService;

    public CaseIndexController(CaseIndexService caseIndexService) {
        this.caseIndexService = caseIndexService;
    }

    @GetMapping("/api/case_indexes")
    public ResponseEntity<?> getAllCaseIndexes(){
        return ResponseEntity.ok(caseIndexService.getAllCaseIndexes());
    }

    @GetMapping("/api/case_indexes/{id}")
    public ResponseEntity<?> getCaseIndexById(@PathVariable long id){
        return ResponseEntity.ok(caseIndexService.getCaseIndexById(id));
    }

    @PostMapping("/api/case_indexes")
    public ResponseEntity<?> createCaseIndex(@RequestBody CaseIndex caseIndex){
        return ResponseEntity.ok(caseIndexService.addCaseIndex(caseIndex));
    }

    @PutMapping("/api/case_indexes/{id}")
    public ResponseEntity<?> updateCaseIndexById(@RequestBody CaseIndex caseIndex, @PathVariable long id){
        return ResponseEntity.ok(caseIndexService.updateCaseIndexById(caseIndex, id));
    }

    @DeleteMapping("/api/case_indexes/{id}")
    public void deleteCaseIndexById(@PathVariable long id){
        caseIndexService.deleteCaseIndexById(id);
    }
}
