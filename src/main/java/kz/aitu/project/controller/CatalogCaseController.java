package kz.aitu.project.controller;


import kz.aitu.project.entity.CatalogCase;
import kz.aitu.project.service.CatalogCaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CatalogCaseController {

    private final CatalogCaseService catalogCaseService;

    public CatalogCaseController(CatalogCaseService catalogCaseService) {
        this.catalogCaseService = catalogCaseService;
    }

    @GetMapping("/api/catalog_cases")
    public List<CatalogCase> getAllCatalogCases(){
        return catalogCaseService.getAllCatalogCasses();
    }

    @GetMapping("/api/catalog_cases/{id}")
    public List<CatalogCase> getCatalogCaseById(@PathVariable long id){
        return catalogCaseService.getCatalogCaseById(id);
    }

    @PostMapping("/api/catalog_cases")
    public ResponseEntity<?> createCatalogCase(@RequestBody CatalogCase catalogCase){
        return ResponseEntity.ok(catalogCaseService.createCatalogCase(catalogCase));
    }

    @PutMapping("/api/catalog_cases/{id}")
    public ResponseEntity<?> updateCatalogCaseById(@RequestBody CatalogCase catalogCase, @PathVariable long id){
        return ResponseEntity.ok(catalogCaseService.updateCatalogCaseById(catalogCase, id));
    }

    @DeleteMapping("/api/catalog_cases/{id}")
    public void deleteCatalogCaseById(@PathVariable long id){
        catalogCaseService.deleteCatalogCaseById(id);
    }
}
