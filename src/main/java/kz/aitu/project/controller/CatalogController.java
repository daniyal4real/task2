package kz.aitu.project.controller;


import kz.aitu.project.entity.Catalog;
import kz.aitu.project.service.CatalogService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CatalogController {

    private final CatalogService catalogService;


    public CatalogController(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @GetMapping("/api/catalogs")
    public ResponseEntity<?> getAllCatalogs(){
        return ResponseEntity.ok(catalogService.getAllCatalogs());
    }

    @GetMapping("/api/catalogs/{id}")
    public List<Catalog> getCatalogById(@PathVariable long id){
        return catalogService.getCatalogById(id);
    }

    @PostMapping("/api/catalogs")
    public ResponseEntity<?> createCatalog(@RequestBody Catalog catalog){
        return ResponseEntity.ok(catalogService.createCatalog(catalog));
    }

    @PutMapping("/api/catalogs/{id}")
    public ResponseEntity<?> updateCatalogById(@RequestBody Catalog catalog, @PathVariable long id){
        return ResponseEntity.ok(catalogService.updateCatalogById(catalog, id));
    }

    @DeleteMapping("/api/catalogs/{id}")
    public void deleteCatalogById(@PathVariable long id){
        catalogService.deleteCatalogById(id);
    }
}
