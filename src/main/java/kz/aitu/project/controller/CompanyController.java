package kz.aitu.project.controller;


import kz.aitu.project.entity.Company;
import kz.aitu.project.service.CompanyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CompanyController {

    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping("/api/companies")
    public List<Company> getAllCompanies(){
        return companyService.getAllCompanies();
    }


    @GetMapping("/api/companies/{id}")
    public List<Company> getCompanyById(@PathVariable long id){
        return companyService.getCompanyById(id);
    }

    @PostMapping("/api/companies")
    public ResponseEntity<?> createCompany(@RequestBody Company company){
        return ResponseEntity.ok(companyService.createCompany(company));
    }

    @PutMapping("/api/companies/{id}")
    public ResponseEntity<?> updateCompanyById(@RequestBody Company company, @PathVariable long id){
        return ResponseEntity.ok(companyService.updateCompanyById(company, id));
    }

    @DeleteMapping("/api/companies/{id}")
    public void deleteCompanyById(@PathVariable long id){
        companyService.deleteCompanyById(id);
    }
}
