package kz.aitu.project.controller;


import kz.aitu.project.entity.CompanyUnit;
import kz.aitu.project.service.CompanyUnitService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CompanyUnitController {

    private final CompanyUnitService companyUnitService;

    public CompanyUnitController(CompanyUnitService companyUnitService) {
        this.companyUnitService = companyUnitService;
    }

    @GetMapping("/api/company_units")
    public List<CompanyUnit> getAllCompanyUnits(){
        return companyUnitService.getAllCompanies();
    }

    @GetMapping("/api/company_units/{id}")
    public List<CompanyUnit> getCompanyUnitById(@PathVariable long id){
        return companyUnitService.getCompanyById(id);
    }

    @PostMapping("/api/company_units")
    public ResponseEntity<?> createCompanyUnit(@RequestBody CompanyUnit companyUnit){
        return ResponseEntity.ok(companyUnitService.createdCompanyUnit(companyUnit));
    }

    @PutMapping("/api/company_units/{id}")
    public ResponseEntity<?> updateCompanyUnitById(@RequestBody CompanyUnit companyUnit, @PathVariable long id){
        return ResponseEntity.ok(companyUnitService.updateCompanyUnitById(companyUnit, id));
    }

    @DeleteMapping("/api/company_units/{id}")
    public void deleteCompanyUnitById(@PathVariable long id){
        companyUnitService.deleteCompanyUnitById(id);
    }
}
