package kz.aitu.project.controller;


import kz.aitu.project.entity.DestructionAct;
import kz.aitu.project.service.DestructionActService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DestructionActController {

    private final DestructionActService destructionActService;

    public DestructionActController(DestructionActService destructionActService) {
        this.destructionActService = destructionActService;
    }

    @GetMapping("/api/destruction_acts")
    public List<DestructionAct> getAllDestructionActs(){
        return destructionActService.getAllDestructionActs();
    }

    @GetMapping("/api/destruction_acts/{id}")
    public List<DestructionAct> getDestructionActById(@PathVariable long id){
        return destructionActService.getDestructionActById(id);
    }

    @PostMapping("/api/destruction_acts")
    public ResponseEntity<?> createDestructionAct(@RequestBody DestructionAct destructionAct){
        return ResponseEntity.ok(destructionActService.createDestructionAct(destructionAct));
    }

    @PutMapping("/api/destruction_acts/{id}")
    public ResponseEntity<?> updateDestructionActById(@RequestBody DestructionAct destructionAct, @PathVariable long id){
        return ResponseEntity.ok(destructionActService.updateDestructionActById(destructionAct, id));
    }

    @DeleteMapping("/api/destruction_acts/{id}")
    public void deleteDestructionActById(@PathVariable long id){
        destructionActService.destructionActService(id);
    }
}
