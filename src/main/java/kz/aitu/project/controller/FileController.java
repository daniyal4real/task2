package kz.aitu.project.controller;


import kz.aitu.project.entity.File;
import kz.aitu.project.service.FileService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FileController {

    private final FileService fileService;


    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping("/api/files")
    public List<File> getAllFiles(){
        return fileService.getAllFiles();
    }

    @GetMapping("/api/files/{id}")
    public List<File> getFileById(@PathVariable long id){
        return fileService.getFileById(id);
    }

    @PostMapping("/api/files")
    public ResponseEntity<?> createFile(@RequestBody File file){
        return ResponseEntity.ok(fileService.createFile(file));
    }

    @PutMapping("/api/files/{id}")
    public ResponseEntity<?> updateFileById(@RequestBody File file, @PathVariable long id){
        return ResponseEntity.ok(fileService.updateFileById(file, id));
    }

    @DeleteMapping("/api/files/{id}")
    public void deleteFileById(@PathVariable long id){
        fileService.deleteFileById(id);
    }
}
