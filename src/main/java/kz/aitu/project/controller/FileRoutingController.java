package kz.aitu.project.controller;


import kz.aitu.project.entity.FileRouting;
import kz.aitu.project.service.FileRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FileRoutingController {

    private final FileRoutingService fileRoutingService;

    public FileRoutingController(FileRoutingService fileRoutingService) {
        this.fileRoutingService = fileRoutingService;
    }

    @GetMapping("/api/file_routings")
    public List<FileRouting> getAllFileRoutings(){
        return fileRoutingService.getAllFileRoutings();
    }

    @GetMapping("/api/file_routings/{id}")
    public List<FileRouting> getFileRoutingById(@PathVariable long id){
        return fileRoutingService.getFileRoutingById(id);
    }

    @PostMapping("/api/file_routings")
    public ResponseEntity<?> createFileRouting(@RequestBody FileRouting fileRouting){
        return ResponseEntity.ok(fileRoutingService.createFileRouting(fileRouting));
    }

    @PutMapping("/api/file_routings/{id}")
    public ResponseEntity<?> updateFileRoutingById(@RequestBody FileRouting fileRouting, @PathVariable long id){
        return ResponseEntity.ok(fileRoutingService.updateFileRoutingById(fileRouting, id));
    }

    @DeleteMapping("/api/file_routings/{id}")
    public void deleteFileRoutingById(@PathVariable long id){
        fileRoutingService.deleteFileRoutingById(id);
    }
}
