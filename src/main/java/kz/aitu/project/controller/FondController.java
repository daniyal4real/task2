package kz.aitu.project.controller;


import kz.aitu.project.entity.Fond;
import kz.aitu.project.service.FondService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FondController {

    private final FondService fondService;

    public FondController(FondService fondService) {
        this.fondService = fondService;
    }

    @GetMapping("/api/fonds")
    public List<Fond> getAllFonds(){
        return fondService.getAllFonds();
    }

    @GetMapping("/api/fonds/{id}")
    public List<Fond> getFondById(@PathVariable long id){
        return fondService.getFondById(id);
    }

    @PostMapping("/api/fonds")
    public ResponseEntity<?> createFond(@RequestBody Fond fond){
        return ResponseEntity.ok(fondService.createFond(fond));
    }

    @PutMapping("/api/fonds/{id}")
    public ResponseEntity<?> updateFondById(@RequestBody Fond fond, @PathVariable long id){
        return ResponseEntity.ok(fondService.updateFondById(fond, id));
    }

    @DeleteMapping("/api/fonds/{id}")
    public void deleteFondById(@PathVariable long id){
        fondService.deleteFondById(id);
    }
}
