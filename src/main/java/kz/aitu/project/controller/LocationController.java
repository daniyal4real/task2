package kz.aitu.project.controller;

import kz.aitu.project.entity.Location;
import kz.aitu.project.service.LocationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LocationController {

    private final LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping("/api/locations")
    public List<Location> getAllLocations(){
        return locationService.getAll();
    }

    @GetMapping("/api/locations/{id}")
    public List<Location> getLocationById(@PathVariable long id){
        return locationService.getById(id);
    }

    @PostMapping("/api/locations")
    public ResponseEntity<?> createLocation(@RequestBody Location location){
        return ResponseEntity.ok(locationService.create(location));
    }

    @PutMapping("/api/locations/{id}")
    public ResponseEntity<?> updateLocationById(@RequestBody Location location, @PathVariable long id){
        return ResponseEntity.ok(locationService.updateById(location,id));
    }

    @DeleteMapping("/api/locations/{id}")
    public void deleteLocationById(@PathVariable long id){
        locationService.deleteLocationById(id);
    }
}
