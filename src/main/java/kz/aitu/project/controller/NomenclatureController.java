package kz.aitu.project.controller;

import kz.aitu.project.entity.Nomenclature;
import kz.aitu.project.service.NomenclatureService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class NomenclatureController {

    private final NomenclatureService nomenclatureService;


    public NomenclatureController(NomenclatureService nomenclatureService) {
        this.nomenclatureService = nomenclatureService;
    }

    @GetMapping("/api/nomenclatures")
    public List<Nomenclature> getAllNomenclatures(){
        return nomenclatureService.getAll();
    }

    @GetMapping("/api/nomenclatures/{id}")
    public List<Nomenclature> getNomenclatureById(@PathVariable long id){
        return nomenclatureService.getById(id);
    }

    @PostMapping("/api/nomenclatures")
    public ResponseEntity<?> createNomenclature(@RequestBody Nomenclature nomenclature){
        return ResponseEntity.ok(nomenclatureService.create(nomenclature));
    }

    @PutMapping("/api/nomenclatures/{id}")
    public ResponseEntity<?> updateNomenclatureById(@RequestBody Nomenclature nomenclature, @PathVariable long id){
        return ResponseEntity.ok(nomenclatureService.updateById(nomenclature, id));
    }

    @DeleteMapping("/api/nomenclatures/{id}")
    public void deleteNomenclatureById(@PathVariable long id){
        nomenclatureService.deleteById(id);
    }
}
