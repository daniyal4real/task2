package kz.aitu.project.controller;

import kz.aitu.project.entity.NomenclatureSummary;
import kz.aitu.project.service.NomenclatureSummaryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class NomenclatureSummaryController {

    private final NomenclatureSummaryService nomenclatureSummaryService;

    public NomenclatureSummaryController(NomenclatureSummaryService nomenclatureSummaryService) {
        this.nomenclatureSummaryService = nomenclatureSummaryService;
    }


    @GetMapping("/api/nomenclature_summaries")
    public List<NomenclatureSummary> getAllNomenclatures(){
        return nomenclatureSummaryService.getAll();
    }

    @GetMapping("/api/nomenclature_summaries/{id}")
    public List<NomenclatureSummary> getNomenclatureById(@PathVariable long id){
        return nomenclatureSummaryService.getById(id);
    }

    @PostMapping("/api/nomenclature_summaries")
    public ResponseEntity<?> createNomenclature(@RequestBody NomenclatureSummary nomenclatureSummary){
        return ResponseEntity.ok(nomenclatureSummaryService.create(nomenclatureSummary));
    }

    @PutMapping("/api/nomenclature_summaries/{id}")
    public ResponseEntity<?> updateNomenclatureById(@RequestBody NomenclatureSummary nomenclatureSummary, @PathVariable long id){
        return ResponseEntity.ok(nomenclatureSummaryService.updateById(nomenclatureSummary, id));
    }

    @DeleteMapping("/api/nomenclature_summaries/{id}")
    public void deleteNomenclatureById(@PathVariable long id){
        nomenclatureSummaryService.deleteById(id);
    }
}
