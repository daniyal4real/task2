package kz.aitu.project.controller;


import kz.aitu.project.entity.Notification;
import kz.aitu.project.service.NotificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class NotificationController {

    private final NotificationService notificationService;


    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }


    @GetMapping("/api/notifications")
    public List<Notification> getAllNotifications(){
        return notificationService.getAll();
    }

    @GetMapping("/api/notifications/{id}")
    public List<Notification> getNotificationById(@PathVariable long id){
        return notificationService.getById(id);
    }

    @PostMapping("/api/notifications")
    public ResponseEntity<?> createNotification(@RequestBody Notification notification){
        return ResponseEntity.ok(notificationService.create(notification));
    }

    @PutMapping("/api/notifications/{id}")
    public ResponseEntity<?> updateNotificationById(@RequestBody Notification notification, @PathVariable long id){
        return ResponseEntity.ok(notificationService.updateById(notification, id));
    }

    @DeleteMapping("/api/notifications/{id}")
    public void deleteNotificationById(@PathVariable long id){
        notificationService.deleteById(id);
    }
}