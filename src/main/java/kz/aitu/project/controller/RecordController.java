package kz.aitu.project.controller;


import kz.aitu.project.entity.Record;
import kz.aitu.project.service.RecordService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RecordController {

    private final RecordService recordService;


    public RecordController(RecordService recordService) {
        this.recordService = recordService;
    }

    @GetMapping("/api/records")
    public List<Record> getAllRecords(){
        return recordService.getAll();
    }

    @GetMapping("/api/records/{id}")
    public List<Record> getRecordById(@PathVariable long id){
        return recordService.getById(id);
    }

    @PostMapping("/api/records")
    public ResponseEntity createRecords(@RequestBody Record record){
        return ResponseEntity.ok(recordService.create(record));
    }

    @PutMapping("/api/records/{id}")
    public ResponseEntity updateRecordById(@RequestBody Record record, @PathVariable long id){
        return ResponseEntity.ok(recordService.updateById(record, id));
    }

    @DeleteMapping("/api/records/{id}")
    public void deleteRecordById(@PathVariable long id){
        recordService.deleteRecordById(id);
    }
}
