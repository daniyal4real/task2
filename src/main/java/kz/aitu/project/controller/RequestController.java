package kz.aitu.project.controller;


import kz.aitu.project.entity.Request;
import kz.aitu.project.service.RequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RequestController {

    private final RequestService requestService;

    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }

    @GetMapping("/api/requests")
    public List<Request> getAllRequests(){
        return requestService.getAll();
    }

    @GetMapping("/api/requests/{id}")
    public List<Request> getRequestById(@PathVariable long id){
        return requestService.getRequestById(id);
    }

    @PostMapping("/api/requests")
    public ResponseEntity<?> createRequest(@RequestBody Request request){
        return ResponseEntity.ok(requestService.create(request));
    }

    @PutMapping("/api/requests/{id}")
    public ResponseEntity<?> updateRequestById(@RequestBody Request request, @PathVariable long id){
        return ResponseEntity.ok(requestService.updateRequestById(request, id));
    }

    @DeleteMapping("/api/requests/{id}")
    public void deleteRequestById(@PathVariable long id){
        requestService.deleteById(id);
    }
}
