package kz.aitu.project.controller;


import kz.aitu.project.entity.RequestStatusHistory;
import kz.aitu.project.service.RequestStatusHistoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RequestStatusHistoryController {

    private final RequestStatusHistoryService requestStatusHistoryService;

    public RequestStatusHistoryController(RequestStatusHistoryService requestStatusHistoryService) {
        this.requestStatusHistoryService = requestStatusHistoryService;
    }

    @GetMapping("/api/request_status_histories")
    public List<RequestStatusHistory> getAllRequestStatusHistories(){
        return requestStatusHistoryService.getAll();
    }

    @GetMapping("/api/request_status_histories/{id}")
    public List<RequestStatusHistory> getRequestStatusHistoryById(@PathVariable long id){
        return requestStatusHistoryService.getRequestStatusHistoryById(id);
    }

    @PostMapping("/api/request_status_histories")
    public ResponseEntity<?> createRequestStatusHistory(@RequestBody RequestStatusHistory requestStatusHistory){
        return ResponseEntity.ok(requestStatusHistoryService.create(requestStatusHistory));
    }

    @PutMapping("/api/request_status_histories/{id}")
    public ResponseEntity<?> updateById(@RequestBody RequestStatusHistory requestStatusHistory, @PathVariable long id){
        return ResponseEntity.ok(requestStatusHistoryService.update(requestStatusHistory, id));
    }

    @DeleteMapping("/api/request_status_histories/{id}")
    public void deleteById(@PathVariable long id){
        requestStatusHistoryService.deleteById(id);
    }
}
