package kz.aitu.project.controller;


import kz.aitu.project.entity.SearchKey;
import kz.aitu.project.service.SearchKeyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SearchKeyController {

    private final SearchKeyService searchKeyService;

    public SearchKeyController(SearchKeyService searchKeyService) {
        this.searchKeyService = searchKeyService;
    }

    @GetMapping("/api/searchkeys")
    public List<SearchKey> getAll(){
        return searchKeyService.getAll();
    }

    @GetMapping("/api/searchkeys/{id}")
    public List<SearchKey> getById(@PathVariable long id){
        return searchKeyService.getById(id);
    }

    @PostMapping("/api/searchkeys")
    public ResponseEntity<?> create(@RequestBody SearchKey searchKey){
        return ResponseEntity.ok(searchKeyService.create(searchKey));
    }

    @PutMapping("/api/searchkeys/{id}")
    public ResponseEntity<?> updateById(@RequestBody SearchKey searchKey, @PathVariable long id){
        return ResponseEntity.ok(searchKeyService.updateById(searchKey, id));
    }

    @DeleteMapping("/api/searchkeys/{id}")
    public void deleteById(@PathVariable long id){
        searchKeyService.deleteById(id);
    }
}
