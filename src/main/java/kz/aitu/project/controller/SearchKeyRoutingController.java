package kz.aitu.project.controller;


import kz.aitu.project.entity.SearchKeyRouting;
import kz.aitu.project.service.SearchKeyRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SearchKeyRoutingController {

    private final SearchKeyRoutingService searchKeyRoutingService;

    public SearchKeyRoutingController(SearchKeyRoutingService searchKeyRoutingService) {
        this.searchKeyRoutingService = searchKeyRoutingService;
    }

    @GetMapping("/api/search_key_routings")
    public List<SearchKeyRouting> getAll(){
        return searchKeyRoutingService.getAll();
    }

    @GetMapping("/api/search_key_routings/{id}")
    public List<SearchKeyRouting> getById(@PathVariable long id){
        return searchKeyRoutingService.getById(id);
    }

    @PostMapping("/api/search_key_routings")
    public ResponseEntity<?> create(@RequestBody SearchKeyRouting searchKeyRouting){
        return ResponseEntity.ok(searchKeyRoutingService.create(searchKeyRouting));
    }

    @PutMapping("/api/search_key_routings/{id}")
    public ResponseEntity<?> updateById(@RequestBody SearchKeyRouting searchKeyRouting, @PathVariable long id){
        return ResponseEntity.ok(searchKeyRoutingService.updateById(searchKeyRouting, id));
    }

    @DeleteMapping("/api/search_key_routings/{id}")
    public void deleteById(@PathVariable long id){
        searchKeyRoutingService.deleteById(id);
    }
}
