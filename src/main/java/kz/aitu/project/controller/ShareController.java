package kz.aitu.project.controller;

import kz.aitu.project.entity.SearchKey;
import kz.aitu.project.entity.Share;
import kz.aitu.project.service.ShareService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ShareController {

    private final ShareService shareService;


    public ShareController(ShareService shareService) {
        this.shareService = shareService;
    }

    @GetMapping("/api/shares")
    public List<Share> getAll(){
        return shareService.getAll();
    }

    @GetMapping("/api/shares/{id}")
    public List<Share> getById(@PathVariable long id){
        return shareService.getById(id);
    }

    @PostMapping("/api/shares")
    public ResponseEntity<?> create(@RequestBody Share share){
        return ResponseEntity.ok(shareService.create(share));
    }

    @PutMapping("/api/shares/{id}")
    public ResponseEntity<?> updateById(@RequestBody Share share, @PathVariable long id){
        return ResponseEntity.ok(shareService.upDateById(share, id));
    }

    @DeleteMapping("/api/shares/{id}")
    public void deleteById(@PathVariable long id){
        shareService.deleteById(id);
    }
}
