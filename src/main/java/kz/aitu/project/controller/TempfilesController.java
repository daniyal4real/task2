package kz.aitu.project.controller;

import kz.aitu.project.entity.Tempfiles;
import kz.aitu.project.service.TempfilesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TempfilesController {

    private final TempfilesService tempfilesService;


    public TempfilesController(TempfilesService tempfilesService) {
        this.tempfilesService = tempfilesService;
    }

    @GetMapping("/api/tempfiles")
    public List<Tempfiles> getAll() {
        return tempfilesService.getAll();
    }

    @GetMapping("/api/tempfiles/{id}")
    public List<Tempfiles> getById(@PathVariable long id){
        return tempfilesService.getById(id);
    }

    @PostMapping("/api/tempfiles")
    public ResponseEntity<?> create(@RequestBody Tempfiles tempfiles){
        return ResponseEntity.ok(tempfilesService.create(tempfiles));
    }

    @PutMapping("/api/tempfiles/{id}")
    public ResponseEntity<?> updateById(@RequestBody Tempfiles tempfiles, @PathVariable long id){
        return ResponseEntity.ok(tempfilesService.updateById(tempfiles, id));
    }

    @DeleteMapping("/api/tempfiles/{id}")
    public void deleteById(@PathVariable long id){
        tempfilesService.deleteById(id);
    }
}
