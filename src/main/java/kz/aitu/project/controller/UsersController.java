package kz.aitu.project.controller;

import kz.aitu.project.entity.Users;
import kz.aitu.project.service.UsersService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UsersController {

    private final UsersService usersService;


    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("/api/users")
    public List<Users> getAll(){
        return usersService.getAll();
    }

    @GetMapping("/api/users/{id}")
    public List<Users> getById(@PathVariable long id){
        return usersService.getById(id);
    }

    @PostMapping("/api/users")
    public ResponseEntity<?> create(@RequestBody Users users){
        return ResponseEntity.ok(usersService.create(users));
    }

    @PutMapping("/api/users/{id}")
    public ResponseEntity<?> updateById(@RequestBody Users users, @PathVariable long id){
        return ResponseEntity.ok(usersService.updateById(users, id));
    }

    @DeleteMapping("/api/users/{id}")
    public void deleteById(@PathVariable long id){
        usersService.deleteById(id);
    }
}
