package kz.aitu.project.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="cases")
public class Case {
    @Id
    private long id;
    private String case_number;
    private String tom_number;
    private String case_header_ru;
    private String case_header_kz;
    private String case_header_en;
    private long start_date;
    private long end_date;
    private long page_quantity;
    private boolean ecp_sign_flag;
    private String ecp_sign;
    private boolean naf_send_sign;
    private boolean delete_sign;
    private boolean restricted_access_flag;
    private String hash;
    private int version;
    private String version_id;
    private boolean active_sign_version;
    private String note;
    private long location_id;
    private long case_index_id;
    private long inventory_id;
    private long destruction_act_id;
    private long unit_structure_id;
    private String blockchain_case_address;
    private long blockchain_addition_date;
    private long creation_date;
    private long created_by;
    private long changed_date;
    private long changed_by;

}
