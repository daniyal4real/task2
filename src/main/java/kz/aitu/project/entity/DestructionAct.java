package kz.aitu.project.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="destruction_act")
public class DestructionAct {
    @Id
    private long id;
    private String act_number;
    private String basis;
    private long structure_unit_id;
    private long creation_date;
    private long created_by;
    private long changed_date;
    private long changed_by;
}
