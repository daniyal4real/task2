package kz.aitu.project.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="nomenclature_summary")
public class NomenclatureSummary {
    @Id
    private long id;
    private String number;
    private int year;
    private long company_unit_id;
    private long created_timestamp;
    private long created_by;
    private long updated_timstamp;
    private long updated_by;
}
