package kz.aitu.project.repository;

import kz.aitu.project.entity.Authorization;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorizationRepository extends CrudRepository <Authorization, Long> {

    List<Authorization> findAll();

    List<Authorization> getAuthorizationById(long id);
}
