package kz.aitu.project.repository;

import kz.aitu.project.entity.CaseIndex;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CaseIndexRepository extends CrudRepository<CaseIndex, Long> {

    List<CaseIndex> findAll();

    List<CaseIndex> getCaseIndexById(long id);
}
