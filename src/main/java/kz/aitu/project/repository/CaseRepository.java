package kz.aitu.project.repository;


import kz.aitu.project.entity.Case;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CaseRepository extends CrudRepository<Case, Long> {
    
    List<Case> findAll();

    List<Case> getCaseById(long id);

}
