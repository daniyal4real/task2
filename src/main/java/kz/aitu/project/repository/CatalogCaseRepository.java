package kz.aitu.project.repository;

import kz.aitu.project.entity.CatalogCase;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CatalogCaseRepository extends CrudRepository<CatalogCase, Long> {
    
    List<CatalogCase> findAll();

    List<CatalogCase> getCatalogCaseById(long id);
}
