package kz.aitu.project.repository;

import kz.aitu.project.entity.Catalog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CatalogRepository extends CrudRepository<Catalog, Long> {

    List<Catalog> findAll();

    List<Catalog> getCatalogById(long id);
}
