package kz.aitu.project.repository;


import kz.aitu.project.entity.Company;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRepository extends CrudRepository<Company, Long> {
    
    List<Company> findAll();

    List<Company> getCompanyById(long id);
}
