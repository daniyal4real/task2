package kz.aitu.project.repository;


import kz.aitu.project.entity.CompanyUnit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyUnitRepository extends CrudRepository<CompanyUnit, Long> {

    List<CompanyUnit> findAll();

    List<CompanyUnit> getCompanyUnitById(long id);
}
