package kz.aitu.project.repository;


import kz.aitu.project.entity.DestructionAct;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DestructionActRepository extends CrudRepository<DestructionAct, Long> {

    List<DestructionAct> findAll();

    List<DestructionAct> getDestructionActById(long id);
}
