package kz.aitu.project.repository;

import kz.aitu.project.entity.File;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface FileRepository extends CrudRepository<File, Long> {
     List<File> findAll() ;

    List<File> getFileById(long id);
}
