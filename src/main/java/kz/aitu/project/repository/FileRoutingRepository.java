package kz.aitu.project.repository;

import kz.aitu.project.entity.FileRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileRoutingRepository extends CrudRepository<FileRouting, Long> {

    List<FileRouting> findAll();

    List<FileRouting> getFileRoutingById(long id);
}
