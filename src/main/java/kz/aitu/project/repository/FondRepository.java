package kz.aitu.project.repository;

import kz.aitu.project.entity.Fond;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FondRepository extends CrudRepository<Fond, Long> {

    List<Fond> findAll();

    List<Fond> getFondById(long id);
}
