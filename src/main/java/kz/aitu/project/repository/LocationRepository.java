package kz.aitu.project.repository;


import kz.aitu.project.entity.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationRepository extends CrudRepository<Location, Long> {

    List<Location>findAll();

    List<Location> getLocationById(long id);
}
