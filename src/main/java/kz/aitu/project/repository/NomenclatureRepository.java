package kz.aitu.project.repository;


import kz.aitu.project.entity.Nomenclature;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NomenclatureRepository extends CrudRepository<Nomenclature, Long> {

    List<Nomenclature> findAll();

    List<Nomenclature> getNomenclatureById(long id);
}
