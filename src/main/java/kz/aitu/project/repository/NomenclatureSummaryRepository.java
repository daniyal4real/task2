package kz.aitu.project.repository;

import kz.aitu.project.entity.NomenclatureSummary;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface NomenclatureSummaryRepository extends CrudRepository<NomenclatureSummary,Long> {

    List<NomenclatureSummary> findAll();

    List<NomenclatureSummary> getNomenclatureById(long id);
}
