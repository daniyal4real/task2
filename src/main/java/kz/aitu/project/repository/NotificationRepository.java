package kz.aitu.project.repository;


import kz.aitu.project.entity.Notification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationRepository extends CrudRepository<Notification, Long> {

    List<Notification>findAll();

    List<Notification> getNotificationById(long id);
}
