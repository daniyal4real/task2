package kz.aitu.project.repository;


import kz.aitu.project.entity.Record;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecordRepository extends CrudRepository<Record, Long> {

    List<Record>findAll();

    List<Record> getRecordById(long id);
}
