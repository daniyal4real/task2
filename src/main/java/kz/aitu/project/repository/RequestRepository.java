package kz.aitu.project.repository;

import kz.aitu.project.entity.Request;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestRepository extends CrudRepository<Request, Long> {

    List<Request> findAll();

    List<Request> getRequestById(long id);
}
