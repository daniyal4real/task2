package kz.aitu.project.repository;


import kz.aitu.project.entity.RequestStatusHistory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestStatusHistoryRepository extends CrudRepository<RequestStatusHistory, Long> {

    List<RequestStatusHistory>findAll();

    List<RequestStatusHistory> getRequestStatusHistoryById(long id);
}
