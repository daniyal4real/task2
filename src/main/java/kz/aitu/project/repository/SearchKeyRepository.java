package kz.aitu.project.repository;

import kz.aitu.project.entity.SearchKey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface SearchKeyRepository extends CrudRepository<SearchKey, Long> {


    List<SearchKey> findAll();

    List<SearchKey> getById(long id);
}
