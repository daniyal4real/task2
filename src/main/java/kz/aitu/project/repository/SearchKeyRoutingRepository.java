package kz.aitu.project.repository;


import kz.aitu.project.entity.SearchKeyRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SearchKeyRoutingRepository extends CrudRepository<SearchKeyRouting, Long> {

    List<SearchKeyRouting> findAll();

    List<SearchKeyRouting> getById(long id);
}
