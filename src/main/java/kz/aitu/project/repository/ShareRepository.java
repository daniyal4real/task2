package kz.aitu.project.repository;

import kz.aitu.project.entity.Share;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShareRepository extends CrudRepository<Share, Long> {

    List<Share> findAll();

    List<Share> getById(long id);
}
