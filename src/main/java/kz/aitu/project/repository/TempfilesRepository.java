package kz.aitu.project.repository;


import kz.aitu.project.entity.Tempfiles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public interface TempfilesRepository extends CrudRepository<Tempfiles, Long> {

    List<Tempfiles> findAll();

    List<Tempfiles> getById(long id);
}
