package kz.aitu.project.repository;

import kz.aitu.project.entity.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepository extends CrudRepository<Users, Long> {

    List<Users> findAll();

    List<Users> getById(long id);
}
