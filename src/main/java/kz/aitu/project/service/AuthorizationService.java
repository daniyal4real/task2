package kz.aitu.project.service;

import kz.aitu.project.entity.Authorization;
import kz.aitu.project.repository.AuthorizationRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorizationService {
    private final AuthorizationRepository authorizationRepository;

    public AuthorizationService(AuthorizationRepository authorizationRepositoty) {
        this.authorizationRepository = authorizationRepositoty;
    }

    public List<Authorization> getAll() {
        return authorizationRepository.findAll();
    }

    public List<Authorization> getAuthorizationById(long id) {
        return authorizationRepository.getAuthorizationById(id);
    }

    public Authorization createAuthorization(Authorization authorization) {
        return authorizationRepository.save(authorization);
    }

    public Authorization updateAuthorization(Authorization authorization, long id){
        return authorizationRepository.save(authorization);
    }

    public void deleteAuthorizationById(long id) {
        authorizationRepository.deleteById(id);
    }
}
