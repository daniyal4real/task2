package kz.aitu.project.service;

import kz.aitu.project.entity.CaseIndex;
import kz.aitu.project.repository.CaseIndexRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CaseIndexService {

    private final CaseIndexRepository caseIndexRepository;

    public CaseIndexService(CaseIndexRepository caseIndexRepository) {
        this.caseIndexRepository = caseIndexRepository;
    }

    public List<CaseIndex> getAllCaseIndexes() {
        return caseIndexRepository.findAll();
    }

    public List<CaseIndex> getCaseIndexById(long id) {
        return caseIndexRepository.getCaseIndexById(id);
    }

    public CaseIndex addCaseIndex(CaseIndex caseIndex) {
        return caseIndexRepository.save(caseIndex);
    }

    public CaseIndex updateCaseIndexById(CaseIndex caseIndex, long id) {
        return caseIndexRepository.save(caseIndex);
    }

    public void deleteCaseIndexById(long id) {
        caseIndexRepository.deleteById(id);
    }
}
