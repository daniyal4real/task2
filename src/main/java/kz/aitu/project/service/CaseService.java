package kz.aitu.project.service;


import kz.aitu.project.entity.Case;
import kz.aitu.project.repository.CaseRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CaseService {

    private final CaseRepository caseRepository;

    public CaseService(CaseRepository caseRepository) {
        this.caseRepository = caseRepository;
    }

    public List<Case> getAllCases() {
        return caseRepository.findAll();
    }

    public List<Case> getCaseById(long id) {
        return caseRepository.getCaseById(id);
    }

    public Case createCase(Case cases){
        return caseRepository.save(cases);
    }

    public Case updateCaseById(Case cases, long id){
        return caseRepository.save(cases);
    }

    public void deleteCaseById(long id) {
        caseRepository.deleteById(id);
    }
}
