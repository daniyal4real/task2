package kz.aitu.project.service;

import kz.aitu.project.entity.Catalog;
import kz.aitu.project.entity.CatalogCase;
import kz.aitu.project.repository.CatalogCaseRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CatalogCaseService {

    private final CatalogCaseRepository catalogCaseRepository;

    public CatalogCaseService(CatalogCaseRepository catalogCaseRepository) {
        this.catalogCaseRepository = catalogCaseRepository;
    }

    public List<CatalogCase> getAllCatalogCasses() {
        return catalogCaseRepository.findAll();
    }

    public List<CatalogCase> getCatalogCaseById(long id) {
        return catalogCaseRepository.getCatalogCaseById(id);
    }

    public CatalogCase createCatalogCase(CatalogCase catalogCase) {
        return catalogCaseRepository.save(catalogCase);
    }

    public CatalogCase updateCatalogCaseById(CatalogCase catalogCase, long id) {
        return catalogCaseRepository.save(catalogCase);
    }

    public void deleteCatalogCaseById(long id) {
        catalogCaseRepository.deleteById(id);
    }
}
