package kz.aitu.project.service;


import kz.aitu.project.entity.Catalog;
import kz.aitu.project.repository.CatalogRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CatalogService {

    private final CatalogRepository catalogRepository;

    public CatalogService(CatalogRepository catalogRepository) {
        this.catalogRepository = catalogRepository;
    }

    public List<Catalog> getAllCatalogs() {
        return catalogRepository.findAll();
    }

    public List<Catalog> getCatalogById(long id) {
        return catalogRepository.getCatalogById(id);
    }

    public Catalog createCatalog(Catalog catalog) {
        return catalogRepository.save(catalog);
    }

    public Catalog updateCatalogById(Catalog catalog, long id) {
        return catalogRepository.save(catalog);
    }

    public void deleteCatalogById(long id) {
        catalogRepository.deleteById(id);
    }
}
