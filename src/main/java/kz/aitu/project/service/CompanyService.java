package kz.aitu.project.service;


import kz.aitu.project.entity.Company;
import kz.aitu.project.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<Company> getAllCompanies() {
        return companyRepository.findAll();
    }

    public List<Company> getCompanyById(long id) {
        return companyRepository.getCompanyById(id);
    }

    public Company createCompany(Company company) {
        return companyRepository.save(company);
    }

    public Company updateCompanyById(Company company, long id) {
        return companyRepository.save(company);
    }

    public void deleteCompanyById(long id) {
        companyRepository.deleteById(id);
    }
}
