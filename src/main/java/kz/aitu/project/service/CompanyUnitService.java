package kz.aitu.project.service;


import kz.aitu.project.entity.CompanyUnit;
import kz.aitu.project.repository.CompanyUnitRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyUnitService {

    private final CompanyUnitRepository companyUnitRepository;

    public CompanyUnitService(CompanyUnitRepository companyUnitRepository) {
        this.companyUnitRepository = companyUnitRepository;
    }

    public List<CompanyUnit> getAllCompanies() {
        return companyUnitRepository.findAll();
    }

    public List<CompanyUnit> getCompanyById(long id) {
        return companyUnitRepository.getCompanyUnitById(id);
    }

    public CompanyUnit createdCompanyUnit(CompanyUnit companyUnit) {
        return companyUnitRepository.save(companyUnit);
    }

    public CompanyUnit updateCompanyUnitById(CompanyUnit companyUnit, long id) {
        return companyUnitRepository.save(companyUnit);
    }

    public void deleteCompanyUnitById(long id) {
        companyUnitRepository.deleteById(id);
    }
}
