package kz.aitu.project.service;

import kz.aitu.project.entity.DestructionAct;
import kz.aitu.project.repository.DestructionActRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DestructionActService {

    private final DestructionActRepository destructionActRepository;

    public DestructionActService(DestructionActRepository destructionActRepository) {
        this.destructionActRepository = destructionActRepository;
    }


    public List<DestructionAct> getAllDestructionActs() {
        return destructionActRepository.findAll();
    }

    public List<DestructionAct> getDestructionActById(long id) {
        return destructionActRepository.getDestructionActById(id);
    }

    public DestructionAct createDestructionAct(DestructionAct destructionAct) {
        return destructionActRepository.save(destructionAct);
    }

    public DestructionAct updateDestructionActById(DestructionAct destructionAct, long id) {
        return destructionActRepository.save(destructionAct);
    }

    public void destructionActService(long id) {
        destructionActRepository.deleteById(id);
    }
}
