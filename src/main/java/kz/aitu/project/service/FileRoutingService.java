package kz.aitu.project.service;

import kz.aitu.project.entity.FileRouting;
import kz.aitu.project.repository.FileRoutingRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileRoutingService {

    private final FileRoutingRepository fileRoutingRepository;

    public FileRoutingService(FileRoutingRepository fileRoutingRepository) {
        this.fileRoutingRepository = fileRoutingRepository;
    }

    public List<FileRouting> getAllFileRoutings() {
        return fileRoutingRepository.findAll();
    }

    public List<FileRouting> getFileRoutingById(long id) {
        return fileRoutingRepository.getFileRoutingById(id);
    }

    public FileRouting createFileRouting(FileRouting fileRouting) {
        return fileRoutingRepository.save(fileRouting);
    }

    public FileRouting updateFileRoutingById(FileRouting fileRouting, long id) {
        return fileRoutingRepository.save(fileRouting);
    }

    public void deleteFileRoutingById(long id) {
        fileRoutingRepository.deleteById(id);
    }
}
