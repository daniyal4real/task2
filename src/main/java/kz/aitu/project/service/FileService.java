package kz.aitu.project.service;

import kz.aitu.project.entity.File;
import kz.aitu.project.repository.FileRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileService {

    private final FileRepository fileRepository;

    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }


    public List<File> getAllFiles() {
        return fileRepository.findAll();
    }

    public List<File> getFileById(long id) {
        return fileRepository.getFileById(id);
    }

    public File createFile(File file) {
        return fileRepository.save(file);
    }


    public Object updateFileById(File file, long id) {
        return fileRepository.save(file);
    }

    public void deleteFileById(long id) {
        fileRepository.deleteById(id);
    }
}
