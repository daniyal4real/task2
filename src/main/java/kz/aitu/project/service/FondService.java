package kz.aitu.project.service;


import kz.aitu.project.entity.Fond;
import kz.aitu.project.repository.FondRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FondService {

    private final FondRepository fondRepository;

    public FondService(FondRepository fondRepository) {
        this.fondRepository = fondRepository;
    }


    public List<Fond> getAllFonds() {
        return fondRepository.findAll();
    }

    public List<Fond> getFondById(long id) {
        return fondRepository.getFondById(id);
    }

    public Fond createFond(Fond fond) {
        return fondRepository.save(fond);
    }

    public Fond updateFondById(Fond fond, long id) {
        return fondRepository.save(fond);
    }

    public void deleteFondById(long id) {
        fondRepository.deleteById(id);
    }
}
