package kz.aitu.project.service;


import kz.aitu.project.entity.Location;
import kz.aitu.project.repository.LocationRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationService {

    private final LocationRepository locationRepository;


    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public List<Location> getAll() {
        return locationRepository.findAll();
    }

    public List<Location> getById(long id) {
        return locationRepository.getLocationById(id);
    }

    public Location create(Location location) {
        return locationRepository.save(location);
    }

    public Location updateById(Location location, long id) {
        return locationRepository.save(location);
    }

    public void deleteLocationById(long id) {
        locationRepository.deleteById(id);
    }
}
