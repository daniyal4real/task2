package kz.aitu.project.service;


import kz.aitu.project.entity.Nomenclature;
import kz.aitu.project.repository.NomenclatureRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NomenclatureService {

    private final NomenclatureRepository nomenclatureRepository;

    public NomenclatureService(NomenclatureRepository nomenclatureRepository) {
        this.nomenclatureRepository = nomenclatureRepository;
    }

    public List<Nomenclature> getAll() {
        return nomenclatureRepository.findAll();
    }

    public List<Nomenclature> getById(long id) {
        return nomenclatureRepository.getNomenclatureById(id);
    }

    public Nomenclature create(Nomenclature nomenclature) {
        return nomenclatureRepository.save(nomenclature);
    }

    public Nomenclature updateById(Nomenclature nomenclature, long id) {
        return nomenclatureRepository.save(nomenclature);
    }

    public void deleteById(long id) {
        nomenclatureRepository.deleteById(id);
    }
}
