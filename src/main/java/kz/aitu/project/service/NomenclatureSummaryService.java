package kz.aitu.project.service;

import kz.aitu.project.entity.Nomenclature;
import kz.aitu.project.entity.NomenclatureSummary;
import kz.aitu.project.repository.NomenclatureSummaryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NomenclatureSummaryService {

    private final NomenclatureSummaryRepository nomenclatureSummaryRepository;

    public NomenclatureSummaryService(NomenclatureSummaryRepository nomenclatureSummaryRepository) {
        this.nomenclatureSummaryRepository = nomenclatureSummaryRepository;
    }


    public List<NomenclatureSummary> getAll() {
        return nomenclatureSummaryRepository.findAll();
    }

    public List<NomenclatureSummary> getById(long id) {
        return nomenclatureSummaryRepository.getNomenclatureById(id);
    }

    public NomenclatureSummary create(NomenclatureSummary nomenclatureSummary) {
        return nomenclatureSummaryRepository.save(nomenclatureSummary);
    }

    public NomenclatureSummary updateById(NomenclatureSummary nomenclatureSummary, long id) {
        return nomenclatureSummaryRepository.save(nomenclatureSummary);
    }

    public void deleteById(long id) {
        nomenclatureSummaryRepository.deleteById(id);
    }
}
