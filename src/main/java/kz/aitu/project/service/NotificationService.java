package kz.aitu.project.service;


import kz.aitu.project.entity.Notification;
import kz.aitu.project.repository.NotificationRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationService {

    private final NotificationRepository notificationRepository;

    public NotificationService(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    public List<Notification> getAll() {
        return notificationRepository.findAll();
    }

    public List<Notification> getById(long id) {
        return notificationRepository.getNotificationById(id);
    }

    public Notification create(Notification notification) {
        return notificationRepository.save(notification);
    }

    public Notification updateById(Notification notification, long id) {
        return notificationRepository.save(notification);
    }

    public void deleteById(long id) {
        notificationRepository.deleteById(id);
    }
}
