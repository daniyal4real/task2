package kz.aitu.project.service;

import kz.aitu.project.entity.Record;
import kz.aitu.project.repository.RecordRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecordService {

    private final RecordRepository recordRepository;

    public RecordService(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }

    public List<Record> getAll() {
        return recordRepository.findAll();
    }

    public List<Record> getById(long id) {
        return recordRepository.getRecordById(id);
    }

    public Record create(Record record) {
        return recordRepository.save(record);
    }

    public Record updateById(Record record, long id) {
        return recordRepository.save(record);
    }

    public void deleteRecordById(long id) {
        recordRepository.deleteById(id);
    }
}
