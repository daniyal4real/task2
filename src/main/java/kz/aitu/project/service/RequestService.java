package kz.aitu.project.service;

import kz.aitu.project.entity.Request;
import kz.aitu.project.repository.RequestRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class RequestService {

    private final RequestRepository requestRepository;

    public RequestService(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    public List<Request> getAll() {
        return requestRepository.findAll();
    }

    public List<Request> getRequestById(long id) {
        return requestRepository.getRequestById(id);
    }

    public Request create(Request request) {
        return requestRepository.save(request);
    }

    public Request updateRequestById(Request request, long id) {
        return requestRepository.save(request);
    }

    public void deleteById(long id) {
        requestRepository.deleteById(id);
    }
}
