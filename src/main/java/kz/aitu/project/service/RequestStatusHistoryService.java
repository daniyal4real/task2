package kz.aitu.project.service;


import kz.aitu.project.controller.RequestStatusHistoryController;
import kz.aitu.project.entity.RequestStatusHistory;
import kz.aitu.project.repository.RequestStatusHistoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RequestStatusHistoryService {

    private  final RequestStatusHistoryRepository requestStatusHistoryRepository;

    public RequestStatusHistoryService(RequestStatusHistoryRepository requestStatusHistoryRepository) {
        this.requestStatusHistoryRepository = requestStatusHistoryRepository;
    }

    public List<RequestStatusHistory> getAll() {
        return requestStatusHistoryRepository.findAll();
    }

    public List<RequestStatusHistory> getRequestStatusHistoryById(long id) {
        return requestStatusHistoryRepository.getRequestStatusHistoryById(id);
    }

    public RequestStatusHistory create(RequestStatusHistory requestStatusHistory) {
        return requestStatusHistoryRepository.save(requestStatusHistory);
    }

    public RequestStatusHistory update(RequestStatusHistory requestStatusHistory, long id) {
        return requestStatusHistoryRepository.save(requestStatusHistory);
    }

    public void deleteById(long id) {
        requestStatusHistoryRepository.deleteById(id);
    }
}
