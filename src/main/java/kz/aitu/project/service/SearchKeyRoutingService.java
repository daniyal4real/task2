package kz.aitu.project.service;

import kz.aitu.project.entity.SearchKeyRouting;
import kz.aitu.project.repository.SearchKeyRoutingRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchKeyRoutingService {

    private final SearchKeyRoutingRepository searchKeyRoutingRepository;

    public SearchKeyRoutingService(SearchKeyRoutingRepository searchKeyRoutingRepository) {
        this.searchKeyRoutingRepository = searchKeyRoutingRepository;
    }

    public List<SearchKeyRouting> getAll() {
        return searchKeyRoutingRepository.findAll();
    }

    public List<SearchKeyRouting> getById(long id) {
        return searchKeyRoutingRepository.getById(id);
    }

    public SearchKeyRouting create(SearchKeyRouting searchKeyRouting) {
        return searchKeyRoutingRepository.save(searchKeyRouting);
    }

    public SearchKeyRouting updateById(SearchKeyRouting searchKeyRouting, long id) {
        return searchKeyRoutingRepository.save(searchKeyRouting);
    }

    public void deleteById(long id) {
        searchKeyRoutingRepository.deleteById(id);
    }
}
