package kz.aitu.project.service;

import kz.aitu.project.entity.SearchKey;
import kz.aitu.project.repository.SearchKeyRepository;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class SearchKeyService {

    private final SearchKeyRepository searchKeyRepository;

    public SearchKeyService(SearchKeyRepository searchKeyRepository) {
        this.searchKeyRepository = searchKeyRepository;
    }


    public List<SearchKey> getAll() {
        return searchKeyRepository.findAll();
    }

    public List<SearchKey> getById(long id) {
        return searchKeyRepository.getById(id);
    }

    public SearchKey create(SearchKey searchKey) {
        return searchKeyRepository.save(searchKey);
    }

    public void deleteById(long id) {
        searchKeyRepository.deleteById(id);
    }

    public SearchKey updateById(SearchKey searchKey, long id) {
        return searchKeyRepository.save(searchKey);
    }
}
