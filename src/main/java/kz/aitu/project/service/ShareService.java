package kz.aitu.project.service;

import kz.aitu.project.entity.SearchKey;
import kz.aitu.project.entity.Share;
import kz.aitu.project.repository.ShareRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShareService {
    private final ShareRepository shareRepository;

    public ShareService(ShareRepository shareRepository) {
        this.shareRepository = shareRepository;
    }

    public List<Share> getAll() {
        return shareRepository.findAll();
    }

    public List<Share> getById(long id) {
        return shareRepository.getById(id);
    }

    public Share create(Share share) {
        return shareRepository.save(share);
    }

    public Share upDateById(Share share, long id) {
        return shareRepository.save(share);
    }

    public void deleteById(long id) {
        shareRepository.deleteById(id);
    }
}
