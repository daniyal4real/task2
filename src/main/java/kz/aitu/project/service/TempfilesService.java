package kz.aitu.project.service;

import kz.aitu.project.entity.Tempfiles;
import kz.aitu.project.repository.TempfilesRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TempfilesService {

    private final TempfilesRepository tempfilesRepository;

    public TempfilesService(TempfilesRepository tempfilesRepository) {
        this.tempfilesRepository = tempfilesRepository;
    }

    public List<Tempfiles> getAll() {
        return tempfilesRepository.findAll();
    }

    public List<Tempfiles> getById(long id) {
        return tempfilesRepository.getById(id);
    }

    public Tempfiles create(Tempfiles tempfiles) {
        return tempfilesRepository.save(tempfiles);
    }

    public Tempfiles updateById(Tempfiles tempfiles, long id) {
        return tempfilesRepository.save(tempfiles);
    }

    public void deleteById(long id) {
        tempfilesRepository.deleteById(id);
    }
}
