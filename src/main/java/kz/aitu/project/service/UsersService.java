package kz.aitu.project.service;

import kz.aitu.project.entity.Users;
import kz.aitu.project.repository.UsersRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersService {

    private final UsersRepository usersRepository;

    public UsersService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public List<Users> getAll() {
        return usersRepository.findAll();
    }

    public List<Users> getById(long id) {
        return usersRepository.getById(id);
    }

    public Users create(Users users) {
        return usersRepository.save(users);
    }

    public Users updateById(Users users, long id) {
        return usersRepository.save(users);
    }

    public void deleteById(long id) {
        usersRepository.deleteById(id);
    }
}
